import mysql from "mysql";

export default function get_sitio(req) {
  //1. definir los parámetros de conexión a mysql

  let conexion;

  let parametros = {
    host: "localhost",
    user: "usuario",
    password: "clave",
    database: "basedatos",
  };
  conexion = mysql.createConnection(parametros);

  //2. Conectarnos al servidor mysql

  conexion.connect(function (err) {
    if (err) {
      console.log("error" + err.message);
      return false;
    } else {
      console.log("Conectado al servidor MySQL");
      return true;
    }
  });

  //3. Realizar consulta SQL
  


  let consulta = `SELECT idplace, name, description FROM place WHERE idplace=? OR idplace=?`;

  conexion.query(consulta,[req.query.a, req.query.b] ,(err, results, fields) => {
    if (err) {
      console.error("error" + err.message);
    } else {
      console.log(results);
    }
  });
  //4. Desconectarnos al servidor mysql

  conexion.end();

  return true;
}
