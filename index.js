//Importar la clase express

import express from 'express';

import agregar_sitio from './agregar_sitio.js';
import get_sitio from './get_sitio.js'


//Crear un objeto express
const app=express();
const puerto=3005;

// Crear una ruta

app.get("/bienvenida", (req, res)=>{

    res.send("Bienvenido al mundo de backend Node.");
});


app.get("/sumar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)+Number(peticion.query.b);

    respuesta.send(resultado.toString());
});

app.get("/agregar_sitio", (req, res)=>{

    res.send(agregar_sitio());
    
});

app.get("/get_lugar", (req, res)=>{


    res.send(console.log(JSON.stringify({idplace:"1",name:"Universidad Distrital Francisco José de Caldas "})));
});





app.get("/consultar_sitio", (req, res)=>{

    res.send(get_sitio(req));
    
});


//Inicializar el servidor node

app.listen(puerto, ()=>{console.log("Esta funcionando el servidor.")});
