import mysql from 'mysql';


export default function agregar_sitio(){

    //1. definir los parámetros de conexión a mysql

    let conexion;

    let parametros = {
        host: "localhost",
        user: "usuario",
        password: "clave",
        database: "basedatos",
      };
    conexion=mysql.createConnection(parametros);

    //2. Conectarnos al servidor mysql

    conexion.connect(function (err){
        if(err){
            console.log("error" + err.message);
            return false;
        }else{
            console.log("Conectado al servidor MySQL");
            return true;
        }
    });

    //3. Realizar consulta SQL

    let consulta=`INSERT INTO place (idplace, name, description, country_code, region, location) 
    VALUES (5,
        'Panadería La última mogolla', 
        'Lo último en delicias',
        'CO', 
        'Bogotá, D.C',
        ST_GeomFromText('POINT(4.611050940917937 -74.07046796181972)')
        ) `;

        conexion.query(consulta);


    //4. Desconectarnos al servidor mysql

    conexion.end();

    return "Insertado!!";



}
